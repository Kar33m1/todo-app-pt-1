import React, { Component } from 'react'
import { TodoItem } from './Todo'


export class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
       
          {this.props.todos.map((todo) => (
            <TodoItem  title={todo.title} completed={todo.completed}
            key={todo.id}
            handleDelete={this.props.handleDelete} 
            id={todo.id}  
            handleToggle={this.props.handleToggle} 
           />
          ))}
        
        </ul>
      </section>
    );
  }
}