import React, { Component } from 'react'

export class TodoItem extends Component {

  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">

          <input className="toggle"  
          type="checkbox" 
          onClick={this.props.handleToggle(this.props.id)}
           />

          <label>{this.props.title}</label>
          <button onClick={this.props.handleDelete(this.props.id)} className="destroy" />
        </div>
      </li>
    );
  }
}