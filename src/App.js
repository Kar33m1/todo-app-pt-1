import React, { Component } from "react";
import todosList from "./todos.json";
import { TodoList } from "./components/TodoList";

class App extends Component {
  constructor() {
    super();

    this.state = {
      todos: todosList,
      newItem: "",
    };
  }

  handleItem = (event) => {
    this.setState({
      newItem: event.target.value,
    });
  };
  
  addItem = (itemText) => {
    const newItem = {
      title: itemText,
      id: Date.now(),
      completed: false
      
    };
    const list = this.state.todos;

    this.setState({ todos: [...list, newItem] });
  };

  handleSubmit = () => {

    this.addItem(this.state.newItem);
    this.setState({ newItem: "" });
  };

  toggleTodo = clicked => event => {
    let list = [...this.state.todos]

    const updatedList = list.map(eachOne => {
      if(eachOne.id === clicked){
       return {
         ...eachOne,
        completed: !eachOne.completed
       }
      } else{
         return eachOne;
       }
      }
    ) 
    this.setState({todos: [...updatedList]})
  }

  destroyItem = id => event => {
    const newList = this.state.todos.filter(
      each => each.id !== id 
    )
    this.setState({ todos: newList})
  }
  //link was helpful with clear complete and Mike Boring helped me with the destroyItem
  //https://www.youtube.com/watch?v=I6IY2TqnPDA&t=1311s
  destroyAll = () =>{
    this.setState({
      todos: this.state.todos.filter(todo => !todo.completed)
    })
  }


  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            type="text"
            value={this.state.newItem}
            placeholder="What needs to be done?"
            onChange={this.handleItem}
            onSubmit={onkeydown = e => {if(e.key ==="Enter"){this.handleSubmit()}}}
          />
        </header>

        <TodoList todos={this.state.todos} handleDelete={this.destroyItem} handleToggle={this.toggleTodo} /> 

        <footer className="footer">
          <span className="todo-count">
    <strong>{this.state.todos.filter(todo => !todo.completed).length}</strong> item(s) left
          </span>
          <button onClick={this.destroyAll} className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }
}
export default App;